import numpy as np
from typing import Iterator
from neural_wrappers.graph_stable import Graph
from neural_wrappers.utilities import npGetInfo
from pathlib import Path
from media_processing_lib.image import tryWriteImage
from matplotlib.cm import hot

from models import WarpFlowGraph, WarpDepthPoseGraph
from models.warp_flow import npWarpFlow
from cycleconcepts.plot_utils import toImageFuncs
from cycleconcepts.nodes import *

cnt = 0

def minMax(x):
	# Min, Max = np.percentile(x, [15, 85])
	# x = np.clip(x, Min, Max)
	return (x - x.min()) / (x.max() - x.min() + np.spacing(1))

def Hot(x):
	return hot(minMax(x))[..., 0 : 3]

def plotFlow(baseDir, taskNode, data, results):
	global cnt

	rgb = data["rgb"]
	rgbPrev = data["rgb(t-1)"]
	GT = data[taskNode.groundTruthKey]
	GTPrev = data["%s(t-1)" % taskNode.groundTruthKey]
	flow = data["optical_flow(t-1, t)"]
	rgbWarp = npWarpFlow(rgbPrev.transpose(0, 3, 1, 2), flow).transpose(0, 2, 3, 1)
	GTWarp = npWarpFlow(GTPrev.transpose(0, 3, 1, 2), flow).transpose(0, 2, 3, 1)
	Pred = results["Single Link(t)"][0]
	PrevPred = results["Single Link(t-1)"][0]
	PredWarp = npWarpFlow(PrevPred.transpose(0, 3, 1, 2), flow).transpose(0, 2, 3, 1)

	diffRGB = Hot(np.abs(rgb - rgbPrev).sum(-1))
	diffRGBWarp = Hot(np.abs(rgb - rgbWarp).sum(-1))
	diffGT = Hot(np.abs(GT - GTPrev).sum(-1))
	diffGTWarp = Hot(np.abs(GT - GTWarp).sum(-1))
	diffPred = Hot(np.abs(Pred - PrevPred).sum(-1))
	diffPredWarp = Hot(np.abs(Pred - PredWarp).sum(-1))

	flow = (flow - 0.5) * 2
	flowMag = np.sqrt(flow[..., 0]**2 + flow[..., 1]**2) / np.sqrt(2)
	flowImgMag = Hot(flowMag)
	flowImgX = Hot(flow[..., 0])
	flowImgY = Hot(flow[..., 1])

	rgb = toImageFuncs[RGB](rgb)
	rgbPrev = toImageFuncs[RGB](rgbPrev)
	rgbWarp = toImageFuncs[RGB](rgbWarp)
	Pred = toImageFuncs[type(taskNode)](Pred)
	PredWarp = toImageFuncs[type(taskNode)](PredWarp)
	PrevPred = toImageFuncs[type(taskNode)](PrevPred)
	GT = toImageFuncs[type(taskNode)](GT)
	GTPrev = toImageFuncs[type(taskNode)](GTPrev)
	GTWarp = toImageFuncs[type(taskNode)](GTWarp)
	flowImgMag = toImageFuncs[RGB](flowImgMag)
	flowImgX = toImageFuncs[RGB](flowImgX)
	flowImgY = toImageFuncs[RGB](flowImgY)
	diffRGB = toImageFuncs[RGB](diffRGB)
	diffRGBWarp = toImageFuncs[RGB](diffRGBWarp)
	diffGT = toImageFuncs[RGB](diffGT)
	diffGTWarp = toImageFuncs[RGB](diffGTWarp)
	diffPred = toImageFuncs[RGB](diffPred)
	diffPredWarp = toImageFuncs[RGB](diffPredWarp)
	# TODO: Would be nice to get metrics/pred/predprev vs GT here :)
	# items = [rgb, rgbPrev, rgbWarp, GT, GTPrev, GTWarp, Pred, PrevPred, PredWarp, \
	# 	flowImgMag, flowImgX, flowImgY, diffRGB, diffRGBWarp, diffGT, diffGTWarp]
	# names = ["rgb", "rgbPrev", "rgbWarp", "GT", "GTPrev", "GTWarp", "Pred", "PrevPred", "PredWarp", \
	# 	"flowImgMag", "flowImgX", "flowImgY", "diffRGB", "diffRGBWarp", "diffGT", "diffGTWarp"]

	# All of them together in one big picture :)
	# TODO: Add title and shit.
	# rgb, rgbPrev, rgbWarp, GT, GTPrev, GTWarp, Pred, PrevPred, PredWarp, \
	# 	flowImgMag, flowImgX, flowImgY, diffRGB, diffRGBWarp, diffGT, diffGTWarp = items
	stack = [
		[rgb, rgbPrev, rgbWarp, diffRGB, diffRGBWarp], \
		[GT, GTPrev, GTWarp, diffGT, diffGTWarp], \
		[Pred, PrevPred, PredWarp, diffPred, diffPredWarp], \
		[flowImgX, flowImgY, flowImgMag, flowImgX * 0, flowImgX * 0]
	]
	stack = [np.concatenate(x, axis=2) for x in stack]
	stack = np.concatenate(stack, axis=1)

	MB = len(rgb)
	for j in range(MB):
		cnt += 1
		# for name, item in zip(names, items):
		# 	tryWriteImage(item[j], "%s/%d_%s.png" % (baseDir, cnt, name))
		tryWriteImage(stack[j], "%s/%d_stack.png" % (baseDir, cnt))

def warp_plot(model:Graph, generator:Iterator, numSteps:int):
	assert isinstance(model, (WarpFlowGraph, WarpDepthPoseGraph))

	if isinstance(model, WarpFlowGraph):
		baseDir = "warp_test_flow"
	else:
		baseDir = "warp_test_depthpose"
	Path(baseDir).mkdir(exist_ok=True)
	taskNode = model.edges[-1].inputNode

	for i in range(numSteps):
		data, labels = next(generator)[0]
		print("Data:")
		for k in data:
			print(" - %s: %s" % (k, npGetInfo(data[k])))

		results, loss = model.mainLoop(data, labels)
		print("Results:", results.keys())
		# for k in results:
		# 	print(" - %s: %s" % (k, [x.shape for x in results[k]]))
		
		if isinstance(model, WarpFlowGraph):
			plotFlow(baseDir, taskNode, data, results)
		else:
			assert False
