import torch as tr

def simpleMedian(self, x):
	x.pop("GT")
	previous = x.pop("Warp(t-1, t)").values()
	current = x.values()

	y = tr.cat([*current, *previous], dim=0)
	return y.median(dim=0)[0].unsqueeze(dim=0)
