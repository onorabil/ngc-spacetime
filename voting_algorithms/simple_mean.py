import torch as tr

def simpleMean(self, x):
	x.pop("GT")
	previous = x.pop("Warp(t-1, t)").values()
	current = x.values()

	y = tr.cat([*current, *previous], dim=0)
	return y.mean(dim=0).unsqueeze(dim=0)