from neural_wrappers.graph_stable import Graph

class WarpFlowGraph(Graph): pass
class WarpDepthPoseGraph(Graph): pass
class WarpFlowGraphEnsemble(Graph): pass
