import torch as tr
from neural_wrappers.graph_stable import Graph, Edge, forwardUseGT, forwardUseIntermediateResult, \
	ReduceNode, MapNode

from cycleconcepts.utils import getModelDataDimsAndHyperParams, fullPath, addNodesArguments
from cycleconcepts.nodes import *
from cycleconcepts.models import *
from cycleconcepts.cfgs.graph_ensemble.utils import meanFunction
from ..warp_delta_pose import warpFunction

class RGBPPrev(RGB):
	def __init__(self):
		super(MapNode, self).__init__(name="RGB (t-1)", groundTruthKey="rgb(t-1)")

class CameraNormalWarpNode(CameraNormal):
	def __init__(self):
		super(MapNode, self).__init__(name="CameraNormal Warp (t-1)", \
			groundTruthKey=["cameranormal", "depth(t-1)", "pose(t-1)", "pose", \
				"cameranormal(t-1)", "rgb", "rgb(t-1)"])

	def getCriterion(self):
		return lambda y, t : None

	def getMetrics(self):
		return {}

def rgb2cameranormal_warp_depth(singleLinksPath:str) -> GraphEnsemble:
	# Graph time T
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	# rgb2cameranormal.loadWeights("%s/rgb2cameranormal/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	# Graph time T-1
	rgbPrevNode = RGBPPrev()
	cameranormalWarpNode = CameraNormalWarpNode()
	rgbPrev2cameranormal = Edge(rgbPrevNode, cameranormalWarpNode, forwardFn=forwardUseGT, blockGradients=True)
	cameranormalWarp = ReduceNode(cameranormalWarpNode, forwardFn=warpFunction)
	# rgbPrev2cameranormal.loadWeights("%s/rgb2cameranormal/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	cameranormalPrevToCurrent = Edge(cameranormalWarpNode, cameranormalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	cameranormalMean = ReduceNode(cameranormalNode, forwardFn=meanFunction)

	graph = Graph([
		# Graph time T
		rgb2cameranormal,
		# Graph time T-1
		rgbPrev2cameranormal,
		cameranormalWarp,
		# Combining them
		cameranormalPrevToCurrent,
		cameranormalMean
	])

	return graph