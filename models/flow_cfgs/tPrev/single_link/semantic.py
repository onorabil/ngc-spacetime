import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph_stable import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2semantic_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and Semantic
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	semanticPrev = PrevNode(semanticNode)
	# Edges: rgb(t)->semantic(t) & rgb(t-1)->semantic(t-1)
	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True, name="Single Link(t)")
	rgbPrev2semantic = Edge(rgbPrevNode, semanticPrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: semantic(t-1) + optical_flow(t-1, t) -> semantic(t)
	semanticPrev2Semantic = Edge(semanticPrev, semanticNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useLoss=False, useMetrics=False)
	# Edge: Vote([semantic(t)]) -> semantic(t)
	voteEdge = ReduceNode(semanticNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2semantic.loadWeights("%s/rgb2semantic/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2semantic.loadWeights("%s/rgb2semantic/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2semantic,
		rgbPrev2semantic,
		semanticPrev2Semantic,
		voteEdge
	])

	return graph