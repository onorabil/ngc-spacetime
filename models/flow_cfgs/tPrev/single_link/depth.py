import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph_stable import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2depth_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and Depth
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	depthNode = Depth(hyperParameters["maxDepthMeters"])
	depthPrev = PrevNode(depthNode)
	# Edges: rgb(t)->depth(t) & rgb(t-1)->depth(t-1)
	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True, name="Single Link(t)")
	rgbPrev2depth = Edge(rgbPrevNode, depthPrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: depth(t-1) + optical_flow(t-1, t) -> depth(t)
	depthPrev2Depth = Edge(depthPrev, depthNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useLoss=False, useMetrics=False)
	# Edge: Vote([depth(t)]) -> depth(t)
	voteEdge = ReduceNode(depthNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2depth.loadWeights("%s/rgb2depth/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2depth.loadWeights("%s/rgb2depth/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2depth,
		rgbPrev2depth,
		depthPrev2Depth,
		voteEdge
	])

	return graph