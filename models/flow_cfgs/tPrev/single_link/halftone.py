import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph_stable import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2halftone_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and Halftone
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	halftoneNode = Halftone()
	halftonePrev = PrevNode(halftoneNode)
	# Edges: rgb(t)->halftone(t) & rgb(t-1)->halftone(t-1)
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True, name="Single Link(t)")
	rgbPrev2halftone = Edge(rgbPrevNode, halftonePrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: halftone(t-1) + optical_flow(t-1, t) -> halftone(t)
	halftonePrev2Halftone = Edge(halftonePrev, halftoneNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useLoss=False, useMetrics=False)
	# Edge: Vote([halftone(t)]) -> halftone(t)
	voteEdge = ReduceNode(halftoneNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2halftone.loadWeights("%s/rgb2halftone/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2halftone.loadWeights("%s/rgb2halftone/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2halftone,
		rgbPrev2halftone,
		halftonePrev2Halftone,
		voteEdge
	])

	return graph