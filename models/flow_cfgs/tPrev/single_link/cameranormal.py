import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph_stable import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2cameranormal_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and CameraNormal
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	cameranormalNode = CameraNormal()
	cameranormalPrev = PrevNode(cameranormalNode)
	# Edges: rgb(t)->cameranormal(t) & rgb(t-1)->cameranormal(t-1)
	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True, \
		name="Single Link(t)")
	rgbPrev2cameranormal = Edge(rgbPrevNode, cameranormalPrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: cameranormal(t-1) + optical_flow(t-1, t) -> cameranormal(t)
	cameranormalPrev2CameraNormal = Edge(cameranormalPrev, cameranormalNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useLoss=False, useMetrics=False)
	# Edge: Vote([cameranormal(t)]) -> cameranormal(t)
	voteEdge = ReduceNode(cameranormalNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2cameranormal.loadWeights("%s/rgb2cameranormal/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2cameranormal.loadWeights("%s/rgb2cameranormal/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2cameranormal,
		rgbPrev2cameranormal,
		cameranormalPrev2CameraNormal,
		voteEdge
	])

	return graph