import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph_stable import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2normal_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and Normal
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	normalNode = Normal()
	normalPrev = PrevNode(normalNode)
	# Edges: rgb(t)->normal(t) & rgb(t-1)->normal(t-1)
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True, name="Single Link(t)")
	rgbPrev2normal = Edge(rgbPrevNode, normalPrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: normal(t-1) + optical_flow(t-1, t) -> normal(t)
	normalPrev2Normal = Edge(normalPrev, normalNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useMetrics=False, useLoss=False)
	# Edge: Vote([normal(t)]) -> normal(t)
	voteEdge = ReduceNode(normalNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2normal.loadWeights("%s/rgb2normal/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2normal.loadWeights("%s/rgb2normal/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2normal,
		rgbPrev2normal,
		normalPrev2Normal,
		voteEdge
	])

	return graph