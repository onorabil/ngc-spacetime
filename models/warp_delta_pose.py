# Mostly stolen from https://github.com/JiawangBian/SC-SfMLearner-Release/blob/master/inverse_warp.py
import os
import numpy as np
import torch
import torch as tr
import torch.nn.functional as F
from neural_wrappers.pytorch import device

pixel_coords = None

def warpFunction(self, x):
	assert "GT" in x
	GT, depthPrev, posePrev, pose, PrevGT, rgb, rgbPrev = x["GT"]
	del x["GT"]

	pose = pose[0]
	posePrev = posePrev[0]

	deltaPoseTranslation = posePrev[:, 0:3] - pose[:, 0:3]
	deltaPoseRotation = posePrev[:, 3:] - pose[:, 0:3]
	deltaPoseRotation = (deltaPoseRotation + 180) % 360 - 180

	trDepthPrev = depthPrev[0, :, :, :, 0] * 300
	trDeltaPose = tr.cat([deltaPoseRotation, deltaPoseTranslation], dim=1)
	trK = tr.from_numpy(computeIntrinsics(resolution=(256, 256))).to(device)

	x = {k : x[k][0] for k in x}
	y = []
	# For each RGB(t-1) -> XXX -> OutputNode
	for k in x:
		thisItem = x[k].permute(0, 3, 1, 2)
		item = warp_delta_pose(thisItem, trDepthPrev, trDeltaPose, trK)
		item = item.permute(0, 2, 3, 1)
		y.append(item)

	# A list of warped representations
	y = tr.stack(y)

	_y = y[0]
	_x = x[tuple(x.keys())[0]]
	rgbWarp = warp_delta_pose(rgbPrev[0].permute(0, 3, 1, 2), trDepthPrev, trDeltaPose, trK).permute(0, 2, 3, 1)
	GTWarp = warp_delta_pose(PrevGT[0].permute(0, 3, 1, 2), trDepthPrev, trDeltaPose, trK).permute(0, 2, 3, 1)
	plot(_x, _y, rgb[0], rgbPrev[0], rgbWarp, GT[0], PrevGT[0], GTWarp, depthPrev[0])
	return y

def meanFunction(self, x):
	if "GT" in x:
		del x["GT"]
	a = tr.cat(list(x.values())).mean(dim=0).unsqueeze(dim=0)
	return a

def set_id_grid(depth):
	global pixel_coords
	b, h, w = depth.size()
	i_range = torch.arange(0, h).view(1, h, 1).expand(
		1, h, w).type_as(depth)  # [1, H, W]
	j_range = torch.arange(0, w).view(1, 1, w).expand(
		1, h, w).type_as(depth)  # [1, H, W]
	ones = torch.ones(1, h, w).type_as(depth)

	pixel_coords = torch.stack((j_range, i_range, ones), dim=1)  # [1, 3, H, W]

def computeIntrinsics(resolution=(512,512), fieldOfView=84):
	fy, fx = resolution / (2 * np.tan(fieldOfView * np.pi / 360))
	cy, cx = np.array(resolution) / 2
	return np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]], dtype=np.float32)

def euler2mat(angle):
	"""Convert euler angles to rotation matrix.
	 Reference: https://github.com/pulkitag/pycaffe-utils/blob/master/rot_utils.py#L174
	Args:
		angle: rotation angle along 3 axis (in radians) -- size = [B, 3]
	Returns:
		Rotation matrix corresponding to the euler angles -- size = [B, 3, 3]
	"""
	B = angle.size(0)
	x, y, z = angle[:, 0], angle[:, 1], angle[:, 2]

	cosz = torch.cos(z)
	sinz = torch.sin(z)

	zeros = z.detach()*0
	ones = zeros.detach()+1
	zmat = torch.stack([cosz, -sinz, zeros,
						sinz,  cosz, zeros,
						zeros, zeros,  ones], dim=1).reshape(B, 3, 3)

	cosy = torch.cos(y)
	siny = torch.sin(y)

	ymat = torch.stack([cosy, zeros,  siny,
						zeros,  ones, zeros,
						-siny, zeros,  cosy], dim=1).reshape(B, 3, 3)

	cosx = torch.cos(x)
	sinx = torch.sin(x)

	xmat = torch.stack([ones, zeros, zeros,
						zeros,  cosx, -sinx,
						zeros,  sinx,  cosx], dim=1).reshape(B, 3, 3)

	rotMat = xmat @ ymat @ zmat
	return rotMat


def pixel2cam(depth, intrinsics_inv):
	global pixel_coords
	"""Transform coordinates in the pixel frame to the camera frame.
	Args:
		depth: depth maps -- [B, H, W]
		intrinsics_inv: intrinsics_inv matrix for each element of batch -- [B, 3, 3]
	Returns:
		array of (u,v,1) cam coordinates -- [B, 3, H, W]
	"""
	b, h, w = depth.size()
	if (pixel_coords is None) or pixel_coords.size(2) < h:
		set_id_grid(depth)
	current_pixel_coords = pixel_coords[:, :, :h, :w].expand(
		b, 3, h, w).reshape(b, 3, -1)  # [B, 3, H*W]
	cam_coords = (intrinsics_inv @ current_pixel_coords).reshape(b, 3, h, w)
	return cam_coords * depth.unsqueeze(1)


def cam2pixel(cam_coords, proj_c2p_rot, proj_c2p_tr, padding_mode):
	"""Transform coordinates in the camera frame to the pixel frame.
	Args:
		cam_coords: pixel coordinates defined in the first camera coordinates system -- [B, 4, H, W]
		proj_c2p_rot: rotation matrix of cameras -- [B, 3, 4]
		proj_c2p_tr: translation vectors of cameras -- [B, 3, 1]
	Returns:
		array of [-1,1] coordinates -- [B, 2, H, W]
	"""
	b, _, h, w = cam_coords.size()
	cam_coords_flat = cam_coords.reshape(b, 3, -1)  # [B, 3, H*W]
	if proj_c2p_rot is not None:
		pcoords = proj_c2p_rot @ cam_coords_flat
	else:
		pcoords = cam_coords_flat

	if proj_c2p_tr is not None:
		pcoords = pcoords + proj_c2p_tr  # [B, 3, H*W]
	X = pcoords[:, 0]
	Y = pcoords[:, 1]
	Z = pcoords[:, 2].clamp(min=1e-3)

	# Normalized, -1 if on extreme left, 1 if on extreme right (x = w-1) [B, H*W]
	X_norm = 2*(X / Z)/(w-1) - 1
	Y_norm = 2*(Y / Z)/(h-1) - 1  # Idem [B, H*W]

	pixel_coords = torch.stack([X_norm, Y_norm], dim=2)  # [B, H*W, 2]
	return pixel_coords.reshape(b, h, w, 2)

def pose_vec2mat(vec, rotation_mode='euler'):
	"""
	Convert 6DoF parameters to transformation matrix.
	Args:s
		vec: 6DoF parameters in the order of tx, ty, tz, rx, ry, rz -- [B, 6]
	Returns:
		A transformation matrix -- [B, 3, 4]
	"""
	translation = vec[:, :3].unsqueeze(-1)  # [B, 3, 1]
	rot = vec[:, 3:]
	if rotation_mode == 'euler':
		rot_mat = euler2mat(rot)  # [B, 3, 3]
	elif rotation_mode == 'quat':
		rot_mat = quat2mat(rot)  # [B, 3, 3]
	transform_mat = torch.cat([rot_mat, translation], dim=2)  # [B, 3, 4]
	return transform_mat

# Warps through depth[t] + pose(t-1 -> t) from img[t-1]
#	imgPrev :: [B, D, H, W] (D = 3 for RGB, but not necessarily)
#	depth :: [B, H, W]
#	posePrev :: [B, 6]
#	pose :: [B, 6]
#	intrinsics :: [B, 3, 3]
# Return
#	warpedImage :: [B, D, H, W]
def npWarpDepthPose(imgPrev:np.ndarray, depth:np.ndarray, posePrev:np.ndarray, pose:np.ndarray, \
	intrinsics:np.ndarray, rotation_mode:str="euler", padding_mode:str="zeros") -> np.ndarray:
	deltaPose = npDeltaPose(posePrev, pose)
	trDeltaPose = tr.from_numpy(deltaPose).to(device)

	trImgPrev = tr.from_numpy(imgPrev).to(device)
	trDepth = tr.from_numpy(depth).to(device)
	trPosePrev = tr.from_numpy(posePrev).to(device)
	trPose = tr.from_numpy(pose).to(device)
	trK = tr.from_numpy(intrinsics).to(device)
	res = warp_depth_pose(trImgPrev, trDepth, trPosePrev, trPose, trK, rotation_mode, padding_mode)
	res = res.detach().to("cpu").numpy()
	return res

# @brief Given two batched absolute positions as 6-DoF (Translation, Rotation), computes the relative pose between
#  them, from pose A to pose B.
# @param[in] pose A The absolute position A. Shape: [B, 6]
# @param[in] pose B The absolute position B. Shape: [B, 6]
# @return a 6-DoF (Translation, Rotation) transformation for the relative pose A -> B. Shape: [B, 6]
def npDeltaPose(poseA:np.ndarray, poseB:np.ndarray):
	translationA, rotationA = poseA[:, 0 : 3], poseA[:, 3 : ]
	translationB, rotationB = poseB[:, 0 : 3], poseB[:, 3 : ]

	deltaPoseTranslation = translationA - translationB
	deltaPoseRotation = (rotationA - rotationB + 180) % 360 - 180
	deltaPose = np.concatenate([deltaPoseRotation, deltaPoseTranslation], axis=1)
	return deltaPoseRotation

def warp_depth_pose(imgPrev:tr.Tensor, depth:tr.Tensor, posePrev:tr.Tensor, pose:tr.Tensor, \
	intrinsics:tr.Tensor, rotation_mode:str='euler', padding_mode:str='zeros') -> tr.Tensor:
	deltaPoseTranslation = posePrev[:, 0:3] - pose[:, 0:3]
	deltaPoseRotation = posePrev[:, 3:] - pose[:, 0:3]
	deltaPoseRotation = (deltaPoseRotation + 180) % 360 - 180
	deltaPose = tr.cat([deltaPoseRotation, deltaPoseTranslation], dim=1)
	return warp_delta_pose(imgPrev, depth, deltaPose, intrinsics, rotation_mode, padding_mode)

def warp_delta_pose(imgPrev:tr.Tensor, depth:tr.Tensor, deltaPose:tr.Tensor, \
	intrinsics:tr.Tensor, rotation_mode:str='euler', padding_mode:str='zeros') -> tr.Tensor:
	"""
	Inverse warp a source image to the target image plane.
	Args:
		imgPrev: the source image (where to sample pixels) -- [B, 3, H, W]
		depth: depth map of the target image -- [B, H, W]
		deltaPose: 6DoF pose parameters from target to source -- [B, 6]
		intrinsics: camera intrinsic matrix -- [B, 3, 3]
	Returns:
		projected_img: Source image warped to the target image plane
		#valid_points: Boolean array indicating point validity
	"""

	batch_size, _, img_height, img_width = imgPrev.size()

	cam_coords = pixel2cam(depth, intrinsics.inverse())  # [B,3,H,W]

	pose_mat = pose_vec2mat(deltaPose, rotation_mode)  # [B,3,4]

	# Get projection matrix for tgt camera frame to source pixel frame
	proj_cam_to_src_pixel = intrinsics @ pose_mat  # [B, 3, 4]

	rot, tr = proj_cam_to_src_pixel[:, :, :3], proj_cam_to_src_pixel[:, :, -1:]
	src_pixel_coords = cam2pixel(
		cam_coords, rot, tr, padding_mode)  # [B,H,W,2]
	projected_img = F.grid_sample(
		imgPrev, src_pixel_coords, padding_mode=padding_mode, align_corners=True)

	#valid_points = src_pixel_coords.abs().max(dim=-1)[0] <= 1

	return projected_img #, valid_points

