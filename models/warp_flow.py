# Mostly stolen from https://github.com/JiawangBian/SC-SfMLearner-Release/blob/master/inverse_warp.py
import os
import numpy as np
import torch
import torch as tr
import torch.nn.functional as F
from neural_wrappers.pytorch import device

def warpFunction(self, x):
	res = {}
	flow = self.fullGT["optical_flow(t-1, t)"]
	for key in x:
		if key == "GT":
			continue

		SLPrev = x[key]
		warpedPrev = SLPrev * 0
		for i in range(len(SLPrev)):
			thisItem = SLPrev[i].permute(0, 3, 1, 2)
			warpedItem = warp_flow(thisItem, flow).permute(0, 2, 3, 1)
			warpedPrev[i] = warpedItem
		res[key] = warpedPrev
	return res

# def warpFunction(self, x):
# 	SLPrev = x["Single Link(t-1)"]
# 	flow = self.fullGT["optical_flow(t-1, t)"]

# 	warpedPrev = SLPrev * 0
# 	for i in range(len(SLPrev)):
# 		thisItem = SLPrev[i].permute(0, 3, 1, 2)
# 		warpedItem = warp_flow(thisItem, flow).permute(0, 2, 3, 1)
# 		warpedPrev[i] = warpedItem
# 	return warpedPrev

def npWarpFlow(img:np.ndarray, flow:np.ndarray, padding_mode:str="zeros") -> np.ndarray:
	trImg = tr.from_numpy(img).to(device)
	trFlow = tr.from_numpy(flow).to(device)
	return warp_flow(trImg, trFlow, padding_mode=padding_mode).detach().to("cpu").numpy()

def warp_flow(img:tr.Tensor, flow:tr.Tensor, padding_mode:str="zeros") -> tr.Tensor:
	"""
	Inverse warp a source image to the target image plane.
	Args:
		img: the source image (where to sample pixels) -- [B, 3, H, W]
		flow: depth map of the target image -- [B, H, W, 2]
	Returns:
		projected_img: Source image warped to the target image plane
	"""

	MB, h, w = flow.shape[0], flow.shape[1], flow.shape[2]
	flow = (flow - 0.5) * 2

	flow_neutral = np.stack(np.meshgrid(np.linspace(-1, 1, w), np.linspace(-1, 1, h)), axis=-1).astype(np.float32)
	flow_neutral = np.repeat(np.expand_dims(flow_neutral, axis=0), MB, axis=0)
	flow_neutral = tr.from_numpy(flow_neutral).to(device)
	flow_torch = flow_neutral - flow
	projected_img = F.grid_sample(img, flow_torch, padding_mode=padding_mode, align_corners=True)

	return projected_img

